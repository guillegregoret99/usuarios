package com.danms.usuarios;


import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.oauth2Login();
        http.authorizeRequests()
                .mvcMatchers(HttpMethod.GET, "/actuator/health").permitAll() // GET requests don't need auth
                .mvcMatchers("/").permitAll()
                .anyRequest().authenticated()
                .and().oauth2Login();
    }
}